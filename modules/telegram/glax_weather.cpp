/**
 * \file
 * \author Mattia Basaglia
 * \copyright Copyright 2015-2018 Mattia Basaglia
 * \section License
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "glax_weather.hpp"

namespace telegram {
namespace glax_weather {

std::string glax_icon(const Settings& parsed)
{
    std::string photo_name;
    int temperature = parsed.get("main.temp", 20.0);
    // https://openweathermap.org/weather-conditions
    std::string icon_code = parsed.get("weather.0.icon", "01d");
    DayNight dn = DayNight(icon_code[2]);
    icon_code.resize(2);
    Condition cond = Condition(std::stoi(icon_code));

    switch ( cond )
    {
        case Clear:
            if ( temperature >= 25 )
                photo_name = "sunny-hot";
            else if ( temperature < 0 )
                photo_name = "freezing";
            else if ( dn == Night )
                photo_name = "night";
            else
                photo_name = "sunny";
            break;
        case FewClouds:
        case ScatteredClouds:
        case BrokenClouds:
            if ( temperature >= 25 )
                photo_name = "sunny-hot";
            else
                photo_name = "cloudy";
            break;
        case ShowerRain:
            photo_name = "rain-00";
            break;
        case Rain:
            photo_name = "rain-";
            if ( dn == Night )
                photo_name += "02-moon";
            else
                photo_name += "01-sun";
        case Thunderstorm:
            photo_name = "rain-10-thunder";
            break;
        case Snow:
            photo_name = "snow";
            break;
        case Fog:
            photo_name = "fog";
            break;
        default:
            photo_name = "not-found";
            break;
    }

    return "https://dragon.best/media/img/weather/" + photo_name + ".png";
}

} // namespace  glax_weather
} // namespace telegram
